# spsrc-slurm-cluster

### Introduction

Install Slurm on CentOS-7 Virtual Cluster.

## Requirements
* 3 VM  with CentOS7.
* 8 GB RAM and 4 CPU cores.
* 50 GB of SSD or another storage technology connected.
* Ansible Control Node or a host with Ansible installed on it and ready for managing remote hosts.

### Cluster Server and Computing Nodes
List of master node and computing nodes within the cluster.

|Hostname           |IP Addr local    | External Access      |
|-------------------|-----------------|----------------------|
|slurm-login        |192.168.250.132  |161.111.167.189:18044 |
|slurm-node-01      |192.168.250.93   |161.111.167.189:18042 |
|slurm-node-02      |192.168.250.25   |161.111.167.189:18043 |


## Use Ansible

You can install a released version of Ansible with pip or a package manager. See our [installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) for details on installing Ansible on a variety of platforms.

Power users and developers can run the devel branch, which has the latest features and fixes, directly. Although it is reasonably stable, you are more likely to encounter 
breaking changes when running the devel branch. We recommend getting involved in the Ansible community if you want to run the devel branch.

## Clone Repo
The first step is to clone the repository on the machine that has ansible installed.
```bash
git clone https://gitlab.com/jsancheziaa/spsrc-slurm-cluster.git
```
## Configuring Inventory File
You need to add the target host in the inventorie
```bash
vi inventories/hosts
```
## To Run Playbook
```bash
ansible-playbook -i inventories/list install-slurm.yml
```
